#!/bin/bash
set -x

[ $# -ne 1 ] && { echo "Usage: $0 RESOURCE_LOG_SUFFIX"; exit 1; }

BASE_DIR=$HOME/thesis/workspace/experiment
LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home
LOG_DIR=$BASE_DIR/data
PID_FILE=$BASE_DIR/tmp/resource.pid

RESOURCE_LOG_SUFFIX="$1"

SITE1_SPACE_GUID="505a776d-a64b-4c6c-a39c-ce622d4e2eb3"
SITE2_SPACE_GUID="077c8c83-f61b-4993-90e3-643c18008f59"
SITE3_SPACE_GUID="398c0d83-33d2-4760-81dc-f0921d4ed02e"

function get_site_instances {
	local site=$1
	export CF_HOME=$CF_HOME_DIR/$site/
	local space_guid=$2
	target_result=$(cf a || true)
	if [[ $target_result == *"FAILED"* ]]; then
		$LOGIN_DIR/$site.sh
	fi
	site_instances=$(cf curl v2/spaces/$space_guid/apps | jq .resources[].entity.instances | jq -s 'add')
}

function log_sum_instances {
	RESOURCE_LOG=$LOG_DIR/"resource_"$RESOURCE_LOG_SUFFIX".csv"
	echo -n "time,instances" > $RESOURCE_LOG
	while true; do
		echo "" >> $RESOURCE_LOG
		echo -n $(date +"%s%3N"), >> $RESOURCE_LOG
		sum_instances=0
		get_site_instances site1 $SITE1_SPACE_GUID
		sum_instances=$(( sum_instances + site_instances ))
		get_site_instances site2 $SITE2_SPACE_GUID
		sum_instances=$(( sum_instances + site_instances ))
		get_site_instances site3 $SITE3_SPACE_GUID
		sum_instances=$(( sum_instances + site_instances ))
		echo -n $sum_instances >> $RESOURCE_LOG
	done;
}

log_sum_instances &
echo $(jobs -pr) >> $PID_FILE
