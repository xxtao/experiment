#!/bin/sh
set -ex

CF_HOME=$HOME/thesis/orange_login/experiment/cf_home/site2

cf create-user-provided-service lizard-eureka-service -p "{\"uri\":\"http://lizard-discovery.cw-vdr-labs1.elpaaso.net\"}"
cf create-user-provided-service lizard-zipkin-service -p "{\"uri\":\"http://lizard-zipkin.cw-vdr-labs1.elpaaso.net\"}"
