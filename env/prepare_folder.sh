#!/bin/sh
set -x

BASE_DIR=$HOME/thesis/workspace/experiment

cd $BASE_DIR
mkdir data
mkdir log
mkdir eval/tmp
mkdir lizard/tmp
mkdir tmp
mkdir -p env/login/cf_home/site1
mkdir -p env/login/cf_home/site2
mkdir -p env/login/cf_home/site3