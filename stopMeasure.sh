#!/bin/bash
set -x

BASE_DIR=$HOME/thesis/workspace/experiment
EVAL_DIR=$BASE_DIR/eval
PID_DIR=$BASE_DIR/tmp

$BASE_DIR/kill.sh $PID_DIR/resource.pid
$EVAL_DIR/stopTest.sh