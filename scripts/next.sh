#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 FINAL_STATE_FILE MID_STATE_FILE"; exit 1; }

export PROTOTYPE_URL=127.0.0.1:8080
export FINAL_STATE_FILE="$1"
export MID_STATE_FILE="$2"

: > $MID_STATE_FILE
http_code=$(curl -w %{http_code} $PROTOTYPE_URL/next -X POST -d @$FINAL_STATE_FILE -H "Content-Type:application/json" -o $MID_STATE_FILE)
cat $MID_STATE_FILE | jq .
if [[ $http_code != 200 ]]; then
  echo "Getting state Failed!"
  exit 1
fi