#!/bin/bash
# export DEBUG=true
export proxy_host=127.0.0.1
export proxy_port=8888
# unset http_proxy https_proxy
cd "$HOME/thesis/workspace/prototype-template-engine/"
export _JAVA_OPTIONS=-Dlogging.level.com.orange=TRACE
# rm -rf target/; mvn package; java -jar target/prototype-template-engine-0.0.1-SNAPSHOT.jar
rm -rf target/; mvn package -DskipTests; java -jar target/prototype-template-engine-0.0.1-SNAPSHOT.jar
