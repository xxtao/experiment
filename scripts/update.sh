#!/bin/bash
set -ex

[ $# -ne 3 ] && { echo "Usage: $0 FINAL_STATE STRATEGY_INITIAL DURATION_LOG"; exit 1; }

STATE_DIR=$HOME/thesis/workspace/experiment/lizard
SCRIPT_DIR=$HOME/thesis/workspace/experiment/scripts
PROTOTYPE_URL="127.0.0.1:8080"

FINAL_STATE="$1"
STRATEGY_INITIAL="$2"
DURATION_LOG="$3"

STRATEGY_CONFIG="$STATE_DIR/$STRATEGY_INITIAL""config.json"
OP_CONFIG="$STATE_DIR/operationConfig.json"
MID_FILE="$STATE_DIR/tmp/mid.json"

echo "$(date +"%s%3N") Start Updating to $FINAL_STATE"

function finish {
	set +e
	$SCRIPT_DIR/verify.sh $FINAL_STATE
	success=$?
	echo "$(date +"%s%3N") Finish Updating to $FINAL_STATE with $success"
	echo -n $(date +"%s%3N"), >> $DURATION_LOG
	echo -n $success >> $DURATION_LOG
}
trap finish EXIT

while true; do
	health=$(curl $PROTOTYPE_URL/health || true)
	if [[ "$health" = "UP" ]]; then 
		break
	fi
	echo "Server is not running!"; 
	sleep 2
done

echo "" >> $DURATION_LOG
echo -n $(basename $FINAL_STATE), >> $DURATION_LOG
echo -n $(date +"%s%3N"), >> $DURATION_LOG

$SCRIPT_DIR/operationConfig.sh $OP_CONFIG
$SCRIPT_DIR/strategyConfig.sh $STRATEGY_INITIAL $STRATEGY_CONFIG

while true; do
	$SCRIPT_DIR/next.sh $FINAL_STATE $MID_FILE
	if [[ -f $MID_FILE && ! -s $MID_FILE ]]; then
        echo "The current state is already the desired state. "
        break
    fi
	$SCRIPT_DIR/push.sh $MID_FILE
	$SCRIPT_DIR/verify.sh $MID_FILE
done