#!/bin/bash

[ $# -ne 1 ] && { echo "Usage: $0 LOG_SUFFIX"; exit 1; }

BASE_DIR=$HOME/thesis/workspace/experiment
PROTOTYPE_DIR=$HOME/thesis/workspace/prototype-template-engine
PID_DIR=$BASE_DIR/tmp
LOG_DIR=$BASE_DIR/log

LOG_SUFFIX=$1
LOG_FILE=$LOG_DIR/server_log_$LOG_SUFFIX.txt

# export DEBUG=true
# export proxy_host=socks-proxy #127.0.0.1
# export proxy_port=8080 #8888
# unset http_proxy https_proxy
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)		env=$BASE_DIR/env/env_linux.sh ;;
    MINGW*)     env=$BASE_DIR/env/env_windows.sh ;;
    *)          exit 1
esac
source $env
export _JAVA_OPTIONS="-Dlogging.level.com.orange=TRACE"

cd $PROTOTYPE_DIR
java -jar target/prototype-template-engine-0.0.1-SNAPSHOT.jar >> $LOG_FILE 2>&1 &
echo $! >> $PID_DIR/server.pid