#!/bin/bash
set -e -x

[ $# -ne 1 ] && { echo "Usage: $0 STATE_FILE"; exit 1; }

export PROTOTYPE_URL="127.0.0.1:8080"
export STATE_FILE="$1"

expected=`curl $PROTOTYPE_URL/is_instantiation -X POST -d @$STATE_FILE -H "Content-Type:application/json"`
if [[ "$expected" != "true" ]]; then
	echo "The current state is not expected."
	exit 1
fi