#!/bin/bash
set -e -x

[ $# -ne 2 ] && { echo "Usage: $0 STRATEGY_INITIAL DEPLOY_CONFIG_FILE"; exit 1; }

date +"%s%3N"

export PROTOTYPE_URL=127.0.0.1:8080

STRATEGY_INITIAL="$1"
DEPLOY_CONFIG_FILE="$2"

case "$STRATEGY_INITIAL" in
	BG)		STRATEGY="BlueGreenStrategy" ;;
	I)		STRATEGY="StraightStrategy" ;;
	C)		STRATEGY="CanaryStrategy" ;;
	EC)		STRATEGY="EconomicCanaryStrategy" ;;
	IS)		STRATEGY="InplaceStrategy" ;;
	*)		exit 1
esac

curl -f $PROTOTYPE_URL/set_strategy_config?strategy=$STRATEGY -X PUT -d @$DEPLOY_CONFIG_FILE -H "Content-Type:application/json"

date +"%s%3N"
exit $?