#!/bin/bash
set -ex

[ $# -ne 1 ] && { echo "Usage: $0 LOG_SUFFIX"; exit 1; }

BASE_DIR=$HOME/thesis/workspace/experiment
STATE_DIR=$BASE_DIR/lizard
SCRIPT_DIR=$BASE_DIR/scripts
DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp

LOG_SUFFIX=$1
TIMESTAMP_DATA=$DATA_DIR/killer_$LOG_SUFFIX.csv

echo "killTime,recoveryTime" > $TIMESTAMP_DATA

while true; do 
	sleep $[ ( $RANDOM % 240 ) + 60 ]s # wait [1, 5) min before kill server
	$SCRIPT_DIR/current.sh $STATE_DIR/sites.json || true
	$BASE_DIR/kill.sh $PID_DIR/server.pid
	echo -n $(date +"%s%3N"), >> $TIMESTAMP_DATA
	echo "Server Killed!"
	sleep $[ ( $RANDOM % 60 ) + 1 ]s # wait [1, 60] sec before restart server
	$SCRIPT_DIR/runServer.sh $LOG_SUFFIX
	echo $(date +"%s%3N") >> $TIMESTAMP_DATA
	echo "Server Recovered!"
done