#!/bin/bash
set -x

[ $# -ne 3 ] && { echo "Usage: $0 FINAL_STATE STRATEGY_INITIAL DURATION_LOG"; exit 1; }

SCRIPT_DIR=$HOME/thesis/workspace/experiment/scripts

update="$SCRIPT_DIR/update.sh $1 $2 $3"

$update
while [ $? -ne 0 ]; do 
	sleep 5
	$update
done