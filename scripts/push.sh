#!/bin/bash
set -e -x

[ $# -ne 1 ] && { echo "Usage: $0 desired_state"; exit 1; }

export PROTOTYPE_URL="127.0.0.1:8080"
export STATE_FILE="$1"

echo "Applying"
date +"%s%3N"
response="./tmp/current_state.json"
:> $response
http_code=$(curl -w %{http_code} $PROTOTYPE_URL/push -X POST -d @$STATE_FILE -H "Content-Type:application/json" -o $response)
date +"%s%3N"
cat $response | jq .
if [[ $http_code != 200 ]]; then
  echo "Applying state change Failed!"
  exit 1
fi