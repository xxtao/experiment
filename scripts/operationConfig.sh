#!/bin/bash
set -e -x

[ $# -ne 1 ] && { echo "Usage: $0 CONFIG_FILE"; exit 1; }

date +"%s%3N"

export PROTOTYPE_URL=127.0.0.1:8080
export CONFIG_FILE="$1"

curl -f $PROTOTYPE_URL/set_operation_config -X PUT -d @$CONFIG_FILE -H "Content-Type:application/json"

date +"%s%3N"
exit $?