#!/bin/bash
set -ex

[ $# -ne 1 ] && { echo "Usage: $0 SITES_FILE"; exit 1; }

export PROTOTYPE_URL=127.0.0.1:8080
export SITES_FILE="$1"

response="./tmp/current_state.json"
:> $response
http_code=$(curl -w %{http_code} $PROTOTYPE_URL/pull -X PUT -d @$SITES_FILE -H "Content-Type:application/json" -o $response)
cat $response | jq .
if [[ $http_code != 200 ]]; then
  echo "Getting state Failed!"
  exit 1
fi