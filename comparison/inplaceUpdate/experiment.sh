#!/bin/bash
set -ex

BASE_DIR=$HOME/thesis/workspace/experiment
SCRIPT_DIR=$BASE_DIR/comparison/inplaceUpdate
DATA_DIR=$BASE_DIR/data

SUFFIX="script"
LOG_SUFFIX=$SUFFIX"_"$(date +%y%m%d-%H%M%S)
DURATION_LOG=$DATA_DIR/duration_$LOG_SUFFIX.csv

function finish {
	$BASE_DIR/stopMeasure.sh
}
trap finish EXIT

$BASE_DIR/measure.sh $SUFFIX
echo -n "Af,starttime,endtime" > $DURATION_LOG
for i in {1..15}; do
	$SCRIPT_DIR/update.sh v2 $DURATION_LOG
	sleep 60
	$SCRIPT_DIR/update.sh v1 $DURATION_LOG
	sleep 60
done 