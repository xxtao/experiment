#!/bin/bash
set -ex

[ $# -ne 1 ] && { echo "Usage: $0 SUFFIX"; exit 1; }

BASE_DIR=$HOME/thesis/workspace/experiment
EVAL_DIR=$BASE_DIR/eval
LOG_DIR=$BASE_DIR/log

SUFFIX="$1"
LOG_SUFFIX=$SUFFIX"_"$(date +%y%m%d-%H%M%S)

$EVAL_DIR/resource.sh $LOG_SUFFIX > $LOG_DIR/resource_log_$LOG_SUFFIX.txt 2>&1
$EVAL_DIR/loadTest.sh $LOG_SUFFIX > $LOG_DIR/jmeter_log_$LOG_SUFFIX.txt 2>&1