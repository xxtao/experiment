#!/bin/bash
set -ex

[ $# -ne 2 ] && { echo "Usage: $0 STRATEGY_INITIAL INTERRUPT_SERVER(true or not)"; exit 1; }

BASE_DIR=$HOME/thesis/workspace/experiment
STATE_DIR=$BASE_DIR/lizard
SCRIPT_DIR=$BASE_DIR/scripts
DATA_DIR=$BASE_DIR/data
PID_DIR=$BASE_DIR/tmp
LOG_DIR=$BASE_DIR/log

STRATEGY_INITIAL="$1"
INTERRUPT_SERVER="$2"

SUFFIX="prototype_"$STRATEGY_INITIAL
if [ "$INTERRUPT_SERVER" = true ] ; then
	SUFFIX=$SUFFIX"_interrupted"
fi
LOG_SUFFIX=$SUFFIX"_"$(date +%y%m%d-%H%M%S)
DURATION_LOG=$DATA_DIR/duration_$LOG_SUFFIX.csv

function init {
	$BASE_DIR/measure.sh $SUFFIX
	$SCRIPT_DIR/runServer.sh $LOG_SUFFIX
	sleep 10
	echo -n "Af,starttime,endtime,success" > $DURATION_LOG
	if [ "$INTERRUPT_SERVER" = true ] ; then
		echo "server kill enabled!"
		$SCRIPT_DIR/serverKiller.sh $LOG_SUFFIX > $LOG_DIR/killer_log_$LOG_SUFFIX.txt 2>&1 &
		echo $! >> $PID_DIR/killer.pid
	fi
}

function finish {
	set +e
	if [ "$INTERRUPT_SERVER" = true ] ; then
		$BASE_DIR/kill.sh $PID_DIR/killer.pid
	fi
	$BASE_DIR/kill.sh $PID_DIR/server.pid
	$BASE_DIR/stopMeasure.sh
}
trap finish EXIT

init
for i in {1..15}; do
	$SCRIPT_DIR/robustUpdate.sh $STATE_DIR/final2.json $STRATEGY_INITIAL $DURATION_LOG
	sleep 60
	$SCRIPT_DIR/robustUpdate.sh $STATE_DIR/final1.json $STRATEGY_INITIAL $DURATION_LOG
	sleep 60
done 