#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)

args = commandArgs(TRUE)
if (length(args) != 3) {
  stop("Three arguments (duration, resource and downtime summary) must be supplied.", call.=FALSE)
} 

dir = "./"
result_dir = "./result"
setwd(dir)

duration_report <- args[1]
resource_report <- args[2]
downtime_report <- args[3]
duration_data <- read.csv(duration_report)
resource_data <- read.csv(resource_report)
downtime_data <- read.csv(downtime_report)

duration_data <- subset(duration_data, method != "BG_interrupted")
resource_data <- subset(resource_data, method != "BG_interrupted")
downtime_data <- subset(downtime_data, method != "BG_i")
duration_data$method <- factor(duration_data$method, levels = c("BG", "Canary", "BGC", "In-place", "In-place Serial", "script"), labels = c("BlueGreen", "Canary", "Mixed", "Straight", "Straight(S)", "script"))
resource_data$method <- factor(resource_data$method, levels = c("BG", "Canary", "BGC", "In-place", "In-place Serial", "script"), labels = c("BlueGreen", "Canary", "Mixed", "Straight", "Straight(S)", "script"))
downtime_data$method <- factor(downtime_data$method, levels = c("BG", "C", "BGC", "IP", "IS","s"), labels = c("BlueGreen", "Canary", "Mixed", "Straight", "Straight(S)", "script"))

duration_diagram <- ggplot(data = duration_data, aes(x = method, y = duration)) + geom_bar(stat = "summary", fun.y = "mean") + labs(title = "update duration", x = "strategy", y = "duration(min)") + theme(title=element_text(size=20), axis.title=element_text(size=16), axis.text.x=element_text(size=11), aspect.ratio=1) #face = "bold"
resource_diagram <- ggplot(data = resource_data, aes(x = method, y = additional_consumption)) + geom_bar(stat = "summary", fun.y = "mean") + labs(title = "additional memory consumption", x = "strategy", y = "memory(Gmin)") + theme(title=element_text(size=18), axis.title=element_text(size=16), axis.text.x=element_text(size=11), aspect.ratio=1)
downtime_diagram <- ggplot(data = downtime_data, aes(x = method, y = downtimeMin)) + geom_bar(stat = "summary", fun.y = "mean") + labs(title = "downtime during update", x = "strategy", y = "downtime(min)") + theme(title=element_text(size=20), axis.title=element_text(size=16), axis.text.x=element_text(size=11), aspect.ratio=1)

diagram_filename <- paste(result_dir, "result.png", sep = "/")
ggarrange(duration_diagram, resource_diagram, downtime_diagram, ncol = 3)
ggsave(diagram_filename,width = 15, height = 5) 
dev.off()