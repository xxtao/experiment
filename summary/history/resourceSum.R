#!/usr/bin/env Rscript

args = commandArgs(TRUE)
if (length(args) != 2) {
  stop("Two or more argument (resource report and duration report) must be supplied.", call.=FALSE)
} 

library(ggplot2)
resource_report = args[1]
timestamp_report = args[2]
dir = "./"
result_dir = "./result"

setwd(dir)
resource_data = read.csv(resource_report)
timestamp = read.csv(timestamp_report)[, c('starttime', 'endtime')]

consumption <- c()
for (i in 1:nrow(timestamp)) {
	resource <- subset(resource_data, time >= timestamp[i,"starttime"] & time <= timestamp[i,"endtime"])
	consumption_data <- 0
	for (j in 2:nrow(resource)) {
		duration <- (resource[j, "time"] - resource[j-1, "time"]) / 1000
		consumption_data <- consumption_data + resource[j, "instances"] * duration
	}
	consumption <- c(consumption, consumption_data)
}

write.csv(data.frame(consumption), file=paste(result_dir, "resource-sum.csv", sep = "/"), quote = FALSE, row.names=FALSE)