#!/bin/bash
set -e

cd "$HOME/thesis/workspace/experiment/data/"
for filename in catalog*; do
	newfilename="${filename/catalog/jmeter_catalog}"
	mv $filename $newfilename
done

for filename in eligibility*; do
	newfilename="${filename/eligibility/jmeter_eligibility}"
	mv $filename $newfilename
done