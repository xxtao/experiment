#!/bin/bash
set -ex

[ $# -ne 0 ] && { echo "Usage: $0"; exit 1; }

BASE_DIR=$HOME/thesis/workspace/experiment
STATE_DIR=$BASE_DIR/lizard
SCRIPT_DIR=$BASE_DIR/scripts
PID_DIR=$BASE_DIR/tmp
LOGIN_DIR=$BASE_DIR/env/login
CF_HOME_DIR=$LOGIN_DIR/cf_home

LOG_SUFFIX="tmp"

function finish {
	$BASE_DIR/kill.sh $PID_DIR/server.pid
}
trap finish EXIT

$SCRIPT_DIR/runServer.sh $LOG_SUFFIX
sleep 10
$SCRIPT_DIR/update.sh $STATE_DIR/final1.json I $BASE_DIR/tmp_duration_prepare.csv

# rename catalog and eligibility
SITE1_SPACE_GUID="505a776d-a64b-4c6c-a39c-ce622d4e2eb3"
SITE2_SPACE_GUID="077c8c83-f61b-4993-90e3-643c18008f59"
SITE3_SPACE_GUID="398c0d83-33d2-4760-81dc-f0921d4ed02e"

function rename_site_apps {
	local site=$1
	export CF_HOME=$CF_HOME_DIR/$site/
	local space_guid=$2
	target_result=$(cf a || true)
	if [[ $target_result == *"FAILED"* ]]; then
		$LOGIN_DIR/$site.sh
	fi
	apps=$(cf curl v2/spaces/$space_guid/apps | jq -r .resources[].entity.name)
	for app in $apps; do
		case $app in
			catalog* )		cf rename $app catalog ;;
			eligibility* )	cf rename $app eligibility ;;
			* ) 			exit 1;
		esac
	done
}

rename_site_apps site1 $SITE1_SPACE_GUID
rename_site_apps site2 $SITE2_SPACE_GUID
rename_site_apps site3 $SITE3_SPACE_GUID