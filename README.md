# Scripts for Experiments

## Commands
- evaluate update prototype with BlueGreen strategy - failure-free case: `./experiment.sh BG F`
- evaluate update prototype with Inplace update strategy - failure-free case: `./experiment.sh I F`
- evaluate update prototype with Canary strategy - failure-free case: `./experiment.sh C F`
- evaluate update prototype with BlueGreen strategy - failure case (i.e. randomly kill update server): `./experiment.sh BG true`
- evaluate script-based automated inplace update with CF (CloudFoundry) CLI: `cd comparison/inplaceUpdate; ./experiment`

## Files structure
```
|- scripts: the scripts for processing updates using the update framework  
|- helloWorld: the architecure description of a simple hello world microservice application  
|- lizard: the architecture description for the lizard microservice application  
|- comparison 
    |- inplaceUpdate: the script processing an update by directly using the PaaS operations  
    |- push2cloud: the scripts for processing an update by using push2cloud operations
|- log: the experiments logs  
|- data: the raw data of the evaluation result  
|- summary: the analysis of evaluation result  
```