#!/bin/bash
set -ex

[ $# -ne 0 ] && { echo "Usage: $0"; exit 1; }

BASE_DIR=$HOME/thesis/workspace/experiment
STATE_DIR=$BASE_DIR/lizard
SCRIPT_DIR=$BASE_DIR/scripts
PID_DIR=$BASE_DIR/tmp
LOG_SUFFIX="tmp"

function finish {
	$BASE_DIR/kill.sh $PID_DIR/server.pid
}
trap finish EXIT

$SCRIPT_DIR/runServer.sh $LOG_SUFFIX
sleep 10
$SCRIPT_DIR/update.sh $STATE_DIR/clean.json I $BASE_DIR/tmp_duration_prepare.csv